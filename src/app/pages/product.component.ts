import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { AppState } from './../app.state';
import { Product } from './../models/product'
import * as ProductActions from './../actions/product.actions';
import { JsonService } from './../services/json.service';

@Component({
  selector: 'product-page',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent {
  title = 'Product';
  constructor(private store: Store<AppState>, private jsonService: JsonService) { 
    this.getProductJSON();
  }

  addProduct(title: string) {
    this.store.dispatch(new ProductActions.AddProduct( {
      id: 'id',
      title: title,
      image: 'image',
      subtitle: 'subtitle',
      brand: 'brand',
      review: null,
      retailer: 'Retailer1',
      details: ['detail1'],
      tags: ['tag1'],
      sales: null
  }
  ))
  }

  getProductJSON()
  {
    this.jsonService.getJSON().subscribe(data => { 
      data.forEach(p => {
        this.store.dispatch(new ProductActions.AddProduct(p));
        });
    }, error => console.log(error));
  }
}
