import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Product } from './../models/product'

export const ADD_PRODUCT       = '[PRODUCT] Add'
export const LOAD_PRODUCT       = '[PRODUCT] Load'
export const LOAD_PRODUCT_SUCCESS       = '[PRODUCT] Load Success'
export const REMOVE_PRODUCT    = '[PRODUCT] Remove'

export class AddProduct implements Action {
    readonly type = ADD_PRODUCT

    constructor(public payload: Product) {}
}
export class LoadProduct implements Action {
    readonly type = LOAD_PRODUCT

    constructor(public payload: Product) {}
}
export class LoadProductSuccess implements Action {
    readonly type = LOAD_PRODUCT_SUCCESS

    constructor(public payload: Product) {}
}
export class RemoveProduct implements Action {
    readonly type = REMOVE_PRODUCT

    constructor(public payload: number) {}
}

export type Actions = AddProduct | RemoveProduct | LoadProduct | LoadProductSuccess