import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ChartModule } from 'angular2-chartjs';

import { TableComponent } from './components/table/table.component';
import { ChartComponent } from './components/chart/chart.component';
import { DetailComponent } from './components/detail/detail.component';

import { ProductComponent } from './pages/product.component';
import { DateFormatPipe } from './pipes/dateFormat.pipe';

import { StoreModule } from '@ngrx/store';
import { reducer } from './reducers/product.reducer';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    TableComponent,
    ChartComponent,
    DetailComponent,
    DateFormatPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ChartModule,
    StoreModule.forRoot({
      product: reducer
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
