import { Review } from './review';
import { Sale } from './sale';

export class Product {
    id: string;
    title: string;
    image: string;
    subtitle: string;
    brand: string;
    review: Review[];
    retailer: string;
    details: string[];
    tags: string[];
    sales: Sale[];
}
