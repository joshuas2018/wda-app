export class Review {
    customer: string;
    review: string;
    score: number;
}