import { Action } from '@ngrx/store'
import { Product } from './../models/product'
import * as ProductActions from './../actions/product.actions'

const initialState: Product = {
    id: 'id',
    title: 'title',
    image: 'image',
    subtitle: 'subtitle',
    brand: 'brand',
    review: null,
    retailer: 'Retailer1',
    details: ['detail1'],
    tags: ['tag1'],
    sales: null
}

export function reducer(state: Product[] = [initialState], action: ProductActions.Actions) {
    switch(action.type) {
        case ProductActions.ADD_PRODUCT:
            return [...state, action.payload];
        case ProductActions.LOAD_PRODUCT:
            return [...state, action.payload];    
        case ProductActions.LOAD_PRODUCT_SUCCESS:
            return [...state, action.payload];
        case ProductActions.REMOVE_PRODUCT:
            state.splice(action.payload, 1)
            return state;
        default:
            return state;
    }
}