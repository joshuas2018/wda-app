import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Product } from './../../models/product';
import { AppState } from './../../app.state';
import * as ProductActions from './../../actions/product.actions';
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  products: Observable<Product[]>;
  type = 'line';
  data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
      {
        label: "My First dataset",
        data: [65, 59, 80, 81, 56, 55, 40]
      }
    ]
  };
  options = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false
   },
   elements: {
    point:{
        radius: 0
    }
   },
   tooltips: {
      enabled: false
   },
   scales: {
    yAxes: [{
      display: false,
      gridLines: {
        drawBorder: false,
      },
    }],xAxes: [{
      gridLines: {
        display: false,
      },
    }],
  },
  };

 monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN",
  "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
];



  constructor(private store: Store<AppState>) { 
    this.products = store.select('product');
    this.products.subscribe(data => {


      let labels  = [];
      let dataSetRetail = [];
      let dataSetWholeSale = [];
      if(data[data.length - 1].sales != null)
      {
        data[data.length - 1].sales.forEach( sale =>
          {
              let date = new Date(sale.weekEnding);
              labels.push(this.monthNames[date.getMonth()])
              dataSetRetail.push(sale.retailSales)
              dataSetWholeSale.push(sale.wholesaleSales)
          });
        let dataAny: any= {
          labels: labels,
          datasets: [
            {
              label: "Retail Sales",
              data: dataSetRetail,
              backgroundColor: 'rgba(10, 29, 95, 1)',
              borderColor: 'rgba(10, 29, 95, 1)',
              borderWidth: 1,
              fill: false
            },
            {
              label: "Wholesale Sales",
              data: dataSetWholeSale,
              backgroundColor: 'rgba(106, 108, 115, 1)',
              borderColor: 'rgba(106, 108, 115, 1)',
              borderWidth: 1,
              fill: false
            }
          ]
        };
        this.data = dataAny;
      }
    });
  }
  ngOnInit() {
  }


}
