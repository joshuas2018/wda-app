import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Product } from './../../models/product';
import { AppState } from './../../app.state';
import * as ProductActions from './../../actions/product.actions';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  products: Observable<Product[]>;
  product: Product = null;
  constructor(private store: Store<AppState>) { 
    this.products = store.select('product');
    this.products.subscribe(data => {
      if(data[data.length - 1] != null)
      {
        this.product = data[data.length - 1];
      }
    });
  }
  ngOnInit() {
  }


}
