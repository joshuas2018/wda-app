import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Product } from './../../models/product';
import { AppState } from './../../app.state';
import * as ProductActions from './../../actions/product.actions';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  products: Observable<Product[]>;
  product: Product;
  constructor(private store: Store<AppState>) { 
    this.products = store.select('product');
    this.products.subscribe(data => {
      if(data[data.length - 1] != null)
      {
        this.product = data[data.length - 1];
      }
    });
  }
  ngOnInit() {
  }


}
