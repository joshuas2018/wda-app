import { Injectable }     from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import { map } from "rxjs/operators";
import { Product } from "../models/product";
@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor(private httpClient: HttpClient) {    
  }

  public getJSON(): Observable<any> {
    return this.httpClient.get("./assets/Webdev_data2.json").pipe(map((response :any) => response));
  }

  public getProductJSON(): Observable<Product> {
    return this.httpClient.get("./assets/Webdev_data2.json").pipe(map((response :any) => response));
  }
}