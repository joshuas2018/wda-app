import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './pages/product.component';

const routes: Routes = [{ path: 'home', component: ProductComponent },
                        { path: '',   redirectTo: '/home', pathMatch: 'full' },  
                        { path: '**', redirectTo: ''}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
