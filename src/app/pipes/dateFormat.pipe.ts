import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

export class DateConstants {
    static readonly DATE_FMT = 'MM-dd-yy';
  }
@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe extends DatePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    return super.transform(new Date(value), DateConstants.DATE_FMT);
  }
}